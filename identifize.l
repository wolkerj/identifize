/*
 * Copyright (C) 2023 Jiří Wolker
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE X CONSORTIUM
 * BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * Except as contained in this notice, the name of <copyright holders> shall not be
 * used in advertising or otherwise to promote the sale, use or other dealings in
 * this Software without prior written authorization from Jiří Wolker.
 */

/*
 * identifize: Add ID attributes to HTML heading without them.
 */

%option noyywrap

%x in_tag

	#include <ctype.h>
	#include <stdbool.h>

	#define MAX_EXTRA_TAGS 1024

	void generate_id(void);

	char *extra_tags = "";
	bool no_header_ids = false;

	int heading_count = 0;
	char current_id[512];

%%

"<"[[:alnum:]]+/">"             |
"<"[[:alnum:]]+/[[:space:]]     |
"<"[[:alnum:]]+$                {
                                        ECHO;
                                        yytext++;
					BEGIN(in_tag);
                                        generate_id();
                                }

<in_tag>^"id="                  |
<in_tag>[:space:]"id="          {
                                        current_id[0] = '\0';
                                        ECHO;
                                }

<in_tag>">"                     {
                                        if (current_id[0] != '\0') {
                                                printf(" id=\"%s\"", current_id);
                                        }
                                        ECHO;
                                        BEGIN(INITIAL);
                                }

%%

unsigned long heading_numbers[6] = { 0, 0, 0, 0, 0, 0 };
unsigned long section_number = 0;
unsigned long tag_numbers[MAX_EXTRA_TAGS];

int main(int argc, char **argv)
{
	extra_tags = "";

	for (int opt; (opt = getopt(argc, argv, "hvHt:")) != -1; ) {
	    	 switch (opt) {
		 case 'h':
			printf(
				"%s -- add ID attributes to HTML document\n"
				"\n"
				"Usage:\n"
				"%s [-hvH] [-t tag1,...] < input.html > output.html\n"
				"\n"
				"Options:\n"
				"\t-h\n"
				"\t\tPrint help information\n"
				"\n"
				"\t-v\n"
				"\t\tPrint version information\n"
				"\n"
				"\t-H\n"
				"\t\tDo not add IDs to headings\n"
				"\n"
				"\t-t tag1,tag2,...,tagN\n"
				"\t\tAdd IDs also to these tags\n"
				"\n",
				argv[0], argv[0]);
			/* FALLTHROUGH */
		case 'v':
			puts(PACKAGE " v" VERSION);
			puts("Project URL: " PACKAGE_URL);
			puts("Send bugreports to " PACKAGE_BUGREPORT);
			return 0;
		case 'H':
			no_header_ids = true;
			break;
		case 't':
			extra_tags = optarg;
			break;
		default:
			return 1;
		}
	}

	int tag_count = 0;
	for (char *ch = extra_tags; *ch != '\0'; ch++) {
		*ch = tolower(*ch);
		if (*ch == ',') {
		   	tag_count++;
			if (tag_count > MAX_EXTRA_TAGS) {
				fprintf(stderr, "%s: too many extra tags\n",
				        argv[0]);
				return 1;
			}
		}
	}

	yylex();
	return 0;
}

void generate_id()
{
	int tag_name_len = yyleng;
	char tag_name[64] = { [63] = '\0' };

	if (tag_name_len > 63) tag_name_len = 63;
	strncpy(tag_name, yytext, tag_name_len);
	for (char *ch = tag_name; *ch != '\0'; ch++) {
		*ch = tolower(*ch);
	}

	if (strlen(tag_name) == 2 && (tag_name[0] == 'h' || tag_name[0] == 'H')
	    && tag_name[1] >= '1' && tag_name[1] <= '6' && !no_header_ids) {
		int level = tag_name[1] - '0';
		for (int pos = level; pos < 6; pos++) {
			heading_numbers[pos] = 0;
		}
		heading_numbers[level - 1]++;
		strcpy(current_id, "heading");
		char *str = current_id + 7;
		for (int pos = 0; pos < level; pos++) {
			str += sprintf(str, "-%lu", heading_numbers[pos]);
		}
	} else {
		/* Try to find the tag. */
		unsigned long tag_n = 0;
		for (char const *tag = extra_tags; tag[0] != '\0'; tag_n++) {
			if (!isalnum(tag[0])) {
				tag++;
				continue;
			}
			char const *end = tag;
			while (isalnum(*end)) end++;
			bool next = false;
			for (char const *ch1 = tag, *ch2 = tag_name;
			     *ch1 != '\0' && *ch1 != ',' && *ch2 != '\0'
			     && !next;
			     ch1++, ch2++) {
				if (*ch1 != *ch2) {
					/* No match. */
					current_id[0] = '\0';
					tag = end;
					next = true;
				}
			}
			if (next) continue;
			/* Match. */
			sprintf(current_id, "%.64s-%lu", tag_name,
			        ++heading_numbers[tag_n]);
			return;
		}
                /* Else we reset the current ID. */
		current_id[0] = '\0';
	}
}